package com.cwk4;

import org.json.JSONObject;

public class JSONMultiply {
    public static Matrix evaluate(String jsonString) {
        JSONObject root = new JSONObject(jsonString);
        final Matrix a = Matrix.fromJSONArray(root.getJSONArray("a"));
        final Matrix b = Matrix.fromJSONArray(root.getJSONArray("b"));
        final Matrix c = a.mul(b);
        return c;
    }
}
