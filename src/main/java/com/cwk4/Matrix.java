package com.cwk4;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Matrix {
    private double[][] elements;
    private int m;
    private int n;
    public Matrix(double[][] e) {
        this.elements = e;
        this.m = e.length;
        this.n = e[0].length;
    }
    public Matrix(int m, int n) {
        this.elements = new double[m][n];
        this.m = m;
        this.n = n;
    }
    public static Matrix zeros(int m, int n) {
        return new Matrix(m, n);
    }
    public static Matrix rand(int m, int n, double from, double to) {
        Matrix mat = Matrix.zeros(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                double val = Math.random() * (to - from) + from;
                mat.set(i, j, val);
            }
        }
        return mat;
    }
    public static Matrix rand(int m, int n) {
        return rand(m, n, 0, 1);
    }
    public void set(int m, int n, double v) {
        elements[m][n] = v;
    }
    
    public double get(int m, int n) {
        return elements[m][n];
    }
    public int numberOfRows() {
        return m;
    }
    public int numberOfColumns() {
        return n;
    }
    public Matrix mul(Matrix o) {
        if (this.numberOfColumns() != o.numberOfRows()) {
            throw new IllegalArgumentException("Number of columns"+
                                               "in A must match number"+
                                               "of rows in B");
        }
        Matrix mat = Matrix.zeros(this.numberOfRows(), o.numberOfColumns());
        for (int i = 0; i < mat.numberOfRows(); i++) {
            for (int j = 0; j < mat.numberOfColumns(); j++) {
                double sum = 0;
                for (int k = 0; k < this.numberOfRows(); k++) {
                    sum += this.get(i, k) * o.get(k, j);
                }
                mat.set(i, j, sum);
            }
        }
        return mat;
    }
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.numberOfRows(); i++) {
            for (int j = 0; j < this.numberOfColumns(); j++) {
                if (j > 0)
                    sb.append("  ");
                String s = String.format("%2.5f", this.get(i,j));
                sb.append(s);
            }
            sb.append("\n");
        }
        return sb.toString();
    }
    public JSONArray toJSONArray() {
        JSONArray rows = new JSONArray(this.numberOfRows());
        for (int i = 0; i < this.numberOfRows(); i++) {
            JSONArray column = new JSONArray(this.numberOfColumns());
            for (int j = 0; j < this.numberOfColumns(); j++) {
                column.put(this.get(i, j));
            }
            rows.put(column);
        }
        return rows;
    }
    static public Matrix fromJSONArray(JSONArray jsonArray) throws JSONException {
        double[][] elements = new double[jsonArray.length()][];
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONArray row = jsonArray.getJSONArray(i);
            elements[i] = new double[row.length()];
            for (int j = 0; j < row.length(); j++) {
                double val = row.getDouble(j);
                elements[i][j] = val;
            }
        }
        return new Matrix(elements);
    }
}
