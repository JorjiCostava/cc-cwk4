package com.cwk4;

import java.util.Scanner;

import java.io.IOException;
import java.io.InputStream;

import com.opencsv.CSVWriter;

import java.io.FileWriter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import org.json.JSONObject;

public class Compare {
    public static String callFaaS(String endpoint, String json) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(endpoint);
        StringEntity postingString = new StringEntity(json);
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        HttpResponse response = httpClient.execute(post);
        InputStream in =  response.getEntity().getContent();
        StringBuilder sb = new StringBuilder();
        try (Scanner sc = new Scanner(in)) {
            while(sc.hasNext())
                sb.append(sc.nextLine());
        }
        return sb.toString();
    }
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Usage: java Compare [endpoint]");
            System.exit(0);
        }

        final int MAX_INPUT_SIZE = 1000;
        final int NUMBER_EXPERIMENTS = 10;
        String[] experiments = new String[MAX_INPUT_SIZE / NUMBER_EXPERIMENTS];

        for (int i = 0; i < NUMBER_EXPERIMENTS; i++) {
            int dim = (MAX_INPUT_SIZE / NUMBER_EXPERIMENTS) * (i + 1);
            Matrix a = Matrix.rand(dim, dim, 0, 100);
            Matrix b = Matrix.rand(dim, dim, 0, 100);
            JSONObject root = new JSONObject();
            root.put("a", a.toJSONArray());
            root.put("b", b.toJSONArray());
            experiments[i] = root.toString();
        }

        final int NUMBER_REPETITIONS = 10;

        Experiment bare = new Experiment(NUMBER_REPETITIONS) {
                @Override
                String getResult(String experiment) {
                    return Function.evaluate(experiment).toString();
                }
            };

        Experiment func = new Experiment(NUMBER_EXPERIMENTS) {
                @Override
                String getResult(String experiment) throws IOException {
                    return callFaaS(args[0], experiment);
                }
            };

        System.out.println("Running experiments");
        CSVWriter writer = new CSVWriter(new FileWriter("output.csv"));
        writer.writeNext(new String[]{
                "input_size", "bare_metal_mean", "bare_metal_std", "func_mean", "func_std"
            });
        for (int i = 0; i < NUMBER_EXPERIMENTS; i++) {
		System.out.println("Experiment "+i);
            bare.run(experiments[i]);
            func.run(experiments[i]);
            String[] line = new String[5];
            line[0] = Integer.toString((MAX_INPUT_SIZE / NUMBER_EXPERIMENTS) * (i + 1));
            line[1] = String.format("%5.10f", bare.getMean());
            line[2] = String.format("%5.10f", bare.getStd());
            line[3] = String.format("%5.10f", func.getMean());
            line[4] = String.format("%5.10f", func.getStd());
            writer.writeNext(line);
        }
        writer.flush();
        System.out.println("Flushed");

    }


    public static void runExperiments(String experiments) {
        
    }
}
