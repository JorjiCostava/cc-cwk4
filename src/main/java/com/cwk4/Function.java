package com.cwk4;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.HttpMethod;
import com.microsoft.azure.functions.HttpRequestMessage;
import com.microsoft.azure.functions.HttpResponseMessage;
import com.microsoft.azure.functions.HttpStatus;
import com.microsoft.azure.functions.OutputBinding;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import com.microsoft.azure.functions.annotation.QueueOutput;
import org.json.*;

import java.util.Optional;

/**
 * Azure Functions with HTTP Trigger.
 */
public class Function {
    /**
     * This function listens at endpoint "/api/HttpExample". Two ways to invoke it using "curl" command in bash:
     * 1. curl -d "HTTP Body" {your host}/api/HttpExample
     * 2. curl "{your host}/api/HttpExample?name=HTTP%20Query"
     */
    public static Matrix evaluate(String jsonString) {
        JSONObject root = new JSONObject(jsonString);
        final Matrix a = Matrix.fromJSONArray(root.getJSONArray("a"));
        final Matrix b = Matrix.fromJSONArray(root.getJSONArray("b"));
        final Matrix c = a.mul(b);
        return c;
    }

    @FunctionName("HttpExample")
    public HttpResponseMessage run(
            @HttpTrigger(
                name = "req",
                methods = {HttpMethod.GET, HttpMethod.POST},
                authLevel = AuthorizationLevel.ANONYMOUS)
                HttpRequestMessage<Optional<String>> request,
            @QueueOutput(name = "msg", queueName = "outqueue", connection = "AzureWebJobsStorage")
            OutputBinding<String> msg, final ExecutionContext context) {
        context.getLogger().info("Java HTTP trigger processed a request.");

        if (!request.getBody().isPresent()) {
            context.getLogger().info("Body = " + request.getBody().isPresent());
            return request.createResponseBuilder(HttpStatus.BAD_REQUEST)
                .body("JSON payload not found.")
                .build();
        }
        else {
            final String body = request.getBody().get();
            try {
                Matrix result = Function.evaluate(body);
                return request.createResponseBuilder(HttpStatus.OK).body(result.toString()).build();
            } catch(JSONException e) {
                return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body(e.toString()).build();
            }


        }

    }
}
