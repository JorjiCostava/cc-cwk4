package com.cwk4;

import java.io.IOException;

abstract class Experiment {
    double[] durations;
    double total = 0.0;
    double mean = 0.0;
    double std = 0.0;
    final int repeat;

    public Experiment(int repeat) {
        this.repeat = repeat;
        this.durations = new double[repeat];
    }

    public void run(String experiment) throws IOException {
        total = 0.0;
        for (int j = 0; j < this.repeat; j++) {
            final long startTime = System.currentTimeMillis();
            String result = this.getResult(experiment);
            final long endTime = System.currentTimeMillis();
            durations[j] = (endTime - startTime) / 1000.0;
            total += durations[j];
            //System.out.println("Result: " + j);
            System.out.println("Repeat: " + j);
        }
        mean = total / durations.length;
        std = 0;
        for (double duration : durations) {
            std += Math.pow(duration - mean, 2);
        }
        std = Math.sqrt(std / durations.length);
    }

    public double getMean() {
        return mean;
    }

    public double getStd() {
        return std;
    }

    abstract String getResult(String experiment) throws IOException;

}
